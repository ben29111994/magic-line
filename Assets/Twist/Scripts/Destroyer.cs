﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class in charge to destroy all the platform who entered in
/// 
/// This script is attached to the GameObject destroyer (child of the GameObject "PlayerParent")
/// </summary>
public class Destroyer : MonoBehaviour 
{
    /// <summary>
    /// Will desatcivate (= despawn) the platform who is triggered with
    /// </summary>
    void OnTriggerStay(Collider other)
    {
        if (this.tag == "Beat" && other.tag == "Player")
        {
            Debug.Log("Beat Detected!");
            StartCoroutine(delayDestroy());
        }
        else if (this.tag == "Beat")
        {
            return;
        }
        else
        {
            other.transform.parent.gameObject.SetActive(false);
        }
    }

    IEnumerator delayDestroy()
    {
        yield return new WaitForSeconds(5);
        Destroy(this.gameObject);
    }
}
