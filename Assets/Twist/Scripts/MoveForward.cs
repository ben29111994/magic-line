﻿using UnityEngine;
using System.Collections;

/// <summary>
/// A class in charge to move forward the player and the camera
/// 
/// This script move forward the GameObject "PlayerParent"
/// 
/// You can change the speed with the variable "sensitivity" in the inspector
/// </summary>
namespace SonicBloom.Koreo
{
    public class MoveForward : MonoBehaviorHelper
    {
        /// <summary>
        /// The speed of the move. Change it if you want to change it
        /// </summary>
        public float sensitivity = 1f;

        //bool isStarted = false;

        //void OnEnable()
        //{
        //    GameManager.OnGameStarted += OnGameStarted;
        //    GameManager.OnGameEnded += OnGameEnded;
        //}

        //void OnDisable()
        //{
        //    GameManager.OnGameStarted -= OnGameStarted;
        //    GameManager.OnGameEnded -= OnGameEnded;
        //}

        //void OnGameStarted()
        //{
        //    isStarted = true;
        //}

        //void OnGameEnded()
        //{
        //    isStarted = false;
        //}

        void Update()
        {
            //if(Input.touchCount > 0 || Input.GetMouseButton(0))
            //{
            //    isStarted = true;
            //}
            if (!Player.isStarted)
                return;

            transform.Translate(Vector3.forward * sensitivity * Time.deltaTime);
        }
    }
}
