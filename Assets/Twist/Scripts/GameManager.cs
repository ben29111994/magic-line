﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


/// <summary>
/// Class in charge of the pooling system (to spawn obstacle prefabs), create new platform each time a platform is despawned and to change the platform colors
/// 
/// This script is attached to the GameObject "GameManager", and is in charge to spawn the platforms during the game, and to change the color of all the platforms
/// 
/// You can change the colors by changing the array "colors" in the inspector
/// </summary>
namespace SonicBloom.Koreo
{
    public class GameManager : MonoBehaviorHelper
    {
        /// <summary>
        /// Create a single instance of the AdsManager
        /// </summary>
        //[SerializeField] private GameObject AdsManagerPrefab;

        public delegate void GameStart();
        public static event GameStart OnGameStarted;

        public delegate void GameEnd();
        public static event GameEnd OnGameEnded;

        /// <summary>
        /// Array of colors used to change the platform color
        /// </summary>
        [SerializeField] private Color[] colors;

        /// <summary>
        /// Material use by all platform. Usefull to change the color
        /// </summary>
        [SerializeField] private Material platformMaterial;

        /// <summary>
        /// The current platform color
        /// </summary>
        [SerializeField] private Color currentPlatformColor;

        /// <summary>
        /// The platform prefab
        /// </summary>
        [SerializeField] private GameObject platformPrefab;

        /// <summary>
        /// List of platformPrefab we will ue during the game
        /// </summary>
        List<GameObject> listPlatform = new List<GameObject>();

        /// <summary>
        /// The position of the last spawned platform
        /// </summary>
        int positionCube = 0;

        /// <summary>
        /// A counter to count the number of spawned platform
        /// </summary>
        int spawnCount = 1;

        System.Random rand = new System.Random();

        /// <summary>
        /// Get the current player lifes
        /// </summary>
        public int GetPlayerLife()
        {
            int l = PlayerPrefs.GetInt("LIFE", 3);
            return l;
        }

        /// <summary>
        /// Set the player lifes
        /// </summary>
        public void SetPlayerLife(int l)
        {
            PlayerPrefs.SetInt("LIFE", l);
            PlayerPrefs.Save();
        }

        /// <summary>
        /// Use one life to continue and decrease by 1 the number of lifes
        /// </summary>
        public void UseOneLife()
        {
            int l = GetPlayerLife();
            l--;
            SetPlayerLife(l);
        }

        public void OnGameStart()
        {
            if (OnGameStarted != null)
                OnGameStarted();
        }

        public void OnGameEnd()
        {
            if (OnGameEnded != null)
                OnGameEnded();
        }

        void Awake()
        {
            int count = 0;

            while (true)
            {
                GameObject o = Instantiate(platformPrefab) as GameObject;

                Transform t = o.transform;
                t.SetParent(transform, true);

                o.SetActive(false);

                listPlatform.Add(o);

                count++;

                if (count > 30)
                    break;
            }

            var adsManager = FindObjectOfType<AdsManager>();

            //if(adsManager == null)
            //{
            //	var o = Instantiate(AdsManagerPrefab) as GameObject;

            //	o.SetActive(true);
            //}
        }

        IEnumerator Start()
        {
            platformMaterial.color = colors[0];

            Application.targetFrameRate = 60;
            GC.Collect();

            yield return 0;

            StartCoroutine(CoroutSpawnCube());
            StartCoroutine(ChangeMaterialColor());
        }

        /// <summary>
        /// Spawned a platform from listPlatform
        /// </summary>
        public Transform SpawnPlatform(int pos)
        {
            Transform t = GetPooledPlatform();

            t.SetParent(transform, true);

            t.position = new Vector3(0, 0, 2 * spawnCount);

            positionCube = pos;

            if (positionCube == 0)
                spawnCount++;

            t.name = spawnCount.ToString();

            t.GetComponent<AnimationCube>().DoPosition();

            return t;
        }


        /// <summary>
        /// Get a platform GameObject from listPlatform. If there is no platform available (so inactive) we create a new one 
        /// </summary>
        Transform GetPooledPlatform()
        {
            var p = listPlatform.Find(o => o.activeInHierarchy == false);

            if (p == null)
            {
                GameObject o = Instantiate(platformPrefab) as GameObject;

                Transform t = o.transform;
                t.SetParent(transform, true);

                o.SetActive(false);

                listPlatform.Add(o);

                p = o;
            }

            p.SetActive(true);

            return p.transform;
        }

        /// <summary>
        /// Coroutine to spawn the cube. 5 at start at position 0. Then Randomly according to the player position (+1 / -1)
        /// </summary>
        IEnumerator CoroutSpawnCube()
        {
            while (true)
            {
                if (positionCube > 5)
                {
                    SpawnPlatform(0);
                }
                else
                {

                    yield return new WaitForSeconds(0.01f);

                    //if (GetRandom() == 0)
                    //    positionCube--;
                    //else
                    positionCube++;
                    Debug.Log(positionCube);
                    var t = SpawnPlatform(positionCube);

                    t.eulerAngles = new Vector3(0, 0, positionCube * 90);
                }

                while (listPlatform.FindAll(o => o.activeInHierarchy == true).Count > 100)
                {
                    yield return 0;
                }
            }
        }


        int GetRandom()
        {

            return rand.Next(0, 2);
        }

        /// <summary>
        /// Change the color of all platforms
        /// </summary>
        IEnumerator ChangeMaterialColor()
        {
            yield return new WaitForSeconds(5f);

            while (true)
            {
                Color colorTemp = colors[UnityEngine.Random.Range(0, colors.Length)];

                StartCoroutine(DoLerp(platformMaterial.color, colorTemp, 3f));

                yield return new WaitForSeconds(10f);
            }


        }

        /// <summary>
        /// Change smoothly the platform color
        /// </summary>
        IEnumerator DoLerp(Color from, Color to, float time)
        {
            float timer = 0;
            while (timer <= time)
            {
                timer += Time.deltaTime;
                platformMaterial.color = Color.Lerp(from, to, timer / time);
                yield return null;
            }
            platformMaterial.color = to;
        }

        void OnDisable()
        {
            platformMaterial.color = colors[0];
        }

        void OnApplicationQuit()
        {
            platformMaterial.color = colors[0];
        }
    }
}
