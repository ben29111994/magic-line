﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class in charge to rotate the Camera with the player when the player jumps
/// This script is attached to the Main Camera (child of the GameObject "PlayerParent")
/// </summary>
namespace SonicBloom.Koreo
{
    public class CameraLogic : MonoBehaviour
    {
        public Transform player;

        void Start()
        {
            var p = FindObjectOfType<Player>();
            player = p.transform;
        }

        void Update()
        {
            transform.rotation = player.rotation;
        }
    }
}
