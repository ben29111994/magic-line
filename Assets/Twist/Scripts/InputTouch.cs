﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;


/// <summary>
/// Class in charge to handle input in the game
/// </summary>
public class InputTouch : MonoBehaviour
{
	public delegate void TouchLeft();
	/// <summary>
	/// Event trigger when touching the screen on the left part
	/// </summary>
	public static event TouchLeft OnTouchLeft;

	public delegate void TouchRight();
	/// <summary>
	/// Event trigger when touching the screen on the right part
	/// </summary>
	public static event TouchRight OnTouchRight;

    //public delegate void SwipeUp();
    //public static event SwipeUp OnSwipeUp;

    //public delegate void SwipeDown();
    //public static event SwipeDown OnSwipeDown;

    //public delegate void SwipeLeft();
    //public static event SwipeLeft OnSwipeLeft;

    //public delegate void SwipeRight();
    //public static event SwipeRight OnSwipeRight;

    public delegate void TouchScreen();
	/// <summary>
	/// Event trigger when touching the screen
	/// </summary>
	public static event TouchScreen OnTouchScreen;

	void Update () 
	{



#if UNITY_TVOS && !UNITY_EDITOR

		if(!gameStarted)
		{
			return;
		}

		_OnTouchScreen();

	

#endif

#if (UNITY_ANDROID || UNITY_IOS || UNITY_TVOS) && !UNITY_EDITOR
		int nbTouches = Input.touchCount;

		if(nbTouches > 0)
		{
			
				Touch touch = Input.GetTouch(0);

				TouchPhase phase = touch.phase;

				if (phase == TouchPhase.Began)
				{
					print("on touch");


			        _OnTouchScreen();
				}		
		}
	
#endif

#if (!UNITY_ANDROID && !UNITY_IOS && !UNITY_TVOS) || UNITY_EDITOR
        //		}
        //		else
        //		{

        if (Input.GetKeyDown (KeyCode.LeftArrow))
		{
			_OnTouchScreen();
		}
		if (Input.GetKeyDown (KeyCode.RightArrow))
		{
            _OnTouchScreen();
        }
		//		}
		#endif
	}

    //void _OnTouchLeft()
    //{
    //	if(OnTouchScreen != null)
    //		OnTouchScreen();

    //	if(OnTouchLeft != null)
    //		OnTouchLeft();
    //}

    //void _OnTouchRight()
    //{
    //	if(OnTouchScreen != null)
    //		OnTouchScreen();

    //	if(OnTouchRight != null)
    //		OnTouchRight();
    //}

    void _OnTouchScreen()
    {
        OnTouchScreen();
    }
}
